function [ output_args ] = ADP2pass( n_iter, prob, adp, restart_soln, ref_policy, tol)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Setup defaults:
%   ADP: 1 sample/pass, bootstrap with max (100, n/10), additive CAVE, 1/n
% Initialize storage
% Loop over # of iterations
%   Identify starting state
%   Sample uncertainty
%   Loop Forward over time
%     Pick decision f(exp_contrib + disc*exp_future_value|decision)
%     Step state forward realizing uncertainty
%     Store realized contrib & value
%     Update contrib function
%     Verify path is still viable
%   Loop Backward over time
%     Handle stepsize
%     Update value function
% Return Results
end

