function results = adpTD1(problem, varargin)
% adpTD1 Temporal Difference Optimization with lamba=1 (double pass)
%
% results = adpTD1(problem, adp_options)
%
% Understanding the four types of states in adpTD1:
%  pre-decision: a description of the system state at the beginning of the
%   time period before a decision has been made. In the ADP toolbox this
%   state only contains the subset of state information required to make a
%   decision.
%  full-state: The ADP toolbox distinguishes a second type of pre-decision
%   state that includes the full set of state dimensions required to
%   simulate the problem, which is a superset of the decision influencing
%   pre-decision state.
%  post-decision: A description of the system state immediately
%   after making a decision, typically the post-decision state has the same
%   form and dimensions as the pre-decision state.
%  vfun: A mapping of the post-decision state that is used for value
%   function approximation. It often contains a sub-set of the
%   post-decision state or a transformed set of alternative dimensions.
%
% State Representation:
%   With the exception of the full-state, all states are represented as
%   numeric row vectors. This allows stacking multiple states to form a 2-D
%   numeric array. Full-states can be any form of valid matlab value which
%   allows maintaining sophisticated Simulink or other simulation data.
%
% Required problem fields (or properties if using problem objects)
%   n_periods
%   dims        with subfields for pre_dec, decision, and post_dec listing the
%                number of dimensions for each.
%   disc_rate   inter-decision period discount rate
%
% Required problem function handle fields (or methods if object-oriented):
%   fSim            Simulates the impact of random processes on the problem
%                    based on the current postdecision state and/or the
%                    current "full" predecision state. The most recent
%                    decision is included for updating the full state.
%                    Returning the next (simplified) pre-decision state and
%                    updated "full" pre-decision state. Also computes the
%                    non-decision contribution of this transition. Function
%                    signature:
%                      [next_pre, sim_contrib, new_full_state]...
%                        = fSim(problem, t, decision, post_state, full_state);
%
%   fApplyDscn      Applies the optimal decision (see functions below) to
%                    convert from the pre-decision to post-decision states.
%                    Function signature:
%                      post_state = fApplyDscn(problem, pre_state, decision, t)
%
%   fFirstState     Return the starting state for the simulation. Function
%                    signature:
%                      [first_pre_state, first_full_state] = fFirstState(problem)
%
% Plus Either:
%   fOptimalDec     A function that finds the optimal decision,
%                    post-decision state and related cost values for a
%                    given pre decision state using the provided value
%                    function. Signature:
%                      [decision, dec_contrib, post_state, forward_val] = ...
%                       fOptimalDec(problem, t, pre_s, vfun, adp_options)
%  or
%   fDecision       A function that lists all possible decisions (as rows
%                    in an arbitrary 2-D array) for the current
%                    pre-decision state. This is used automatically by
%                    FindOptDecFromVfun() to exhaustively search the
%                    decision space. Function signature:
%                      d_list = problem.fDecision(problem, pre_s, t)
%
% In addition the following function fields (or methods) can optionally be
% defined for problem specific behavior
%   fPostToVfun     Takes  a set of post decision states (one state per
%                    row) and converts it into a different set of states
%                    (still one per row) for use with the value function.
%                    This allows using alternative bases (e.g. principle
%                    components) or simply a subset of the dimensions. If
%                    omitted, the all postdecision state dimensions are
%                    used. Function signature:
%                      vfun_state_list = fPostToVfun(problem, post_state_list, t)
%   fFullSim        Simulate all forward paths in their entirety based on
%                    a list of decisions obtained during the forward pass
%                    based on the value function. This is useful in two
%                    cases:
%                       1) With separate sub-model approximation, the
%                       current approximation is used during the forward
%                       pass, the full simulations are run, and then the
%                       sub-model approximation & associated contributions
%                       can be updated simultaneously. This is particularly
%                       helpful for expensive sub-models by enabling better
%                       balancing and non-duplication in parallel sub-model
%                       runs.
%
%                       2) If it is possible to run an approximate,
%                       incremental forward simulation with fSim, but the
%                       full sophisticated model requires simulating the
%                       entire time horizon all at once
%                    The function takes/returns 3d values with one row per
%                    time period, one column per associated dimension (eg
%                    each state dimension), and a 3rd dimension with one
%                    entry per parallel path. Function signature:
%                      [sim_contrib_array, problem] = ...
%                        fFullSim(problem, t, pre_state_3d, decision_3d, sim_contrib_3d, post_state_3d);
%                    For complete simulations over all time, t is specified
%                    as [].
%
%   fDecCompare     Comparision function for decisions used for
%                    convergence checks and backpass abort calculations.
%                    Default: isequaln(). Function signature:
%                      t_or_f = fDecCompare(new_decision, old_decision)
%   fValCompare     Comparision function for value functions used for
%                    convergence checks and backpass abort calculations.
%                    Default: simple internal relative value compare with
%                    divide by zero work around. Function signature:
%                      t_or_f_list = fValCompare(new_val_list, old_val_list, tol)
%                    The associate tolerance is set by the 'tol_bkps_val'
%                    adp option
%
% Adp options:
%   See ADP defaults definition. Any of these can be overridden by
%   supplying a string value pair as optional arguements or by passing the
%   corresponding structure or cell array form.

% HISTORY
% ver     date    time       who     changes made
% ---  ---------- -----  ----------- ---------------------------------------
%   1  2012-03           BryanP      Pseudo-code
%   2  2012-04           BryanP      Original (partial) Code
%   3  2012-04-16 17:25  BryanP      First complete version
%   4  2012-04-20 06:45  BryanP      Separate postdec from vfun state spaces
%   5  2012-04-21 21:00  BryanP      Reordered indicies with s_path last to enable easier slicing
%   6  2012-04-22 17:30  BryanP      IT WORKS! Corrected backpass value track/update, add obj & p1_desc to results
%   7  2012-05-05 23:50  BryanP      Added support for consistant random numbers (fix_rand)
%   8  2012-05-05 23:55  BryanP      Add adp to results
%   9  2012-05-07 11:22  BryanP      Add dimension names to vfun
%  10  2012-05-08 14:05  BryanP      Renamed from PreToPost to ApplyDscn
%  11  2012-05-11 17:10  BryanP      Cleaned-up discounting
%  12  2012-05-14 12:40  BryanP      Renamed adpTD1 (from parTD1)
%  13  2012-05-15 01:30  BryanP      Corrected 2nd-to-last contribution in back pass
%  14  2012-06-08 12:55  BryanP      New adp defaults: decfun_params
%  15  2012-06-12 11:05  BryanP      Interface clean up:
%                                     - Switch to varargin based options.
%                                     - Move old_results to options
%                                     - Updated comments
%  16  2012-06-25 21:35  BryanP      updated for renamed SetDefaultOpts (was SetOptions in adp svn <86)
%  17  2012-07-03 14:55  BryanP      Renamed adpSBI (was adpSampBackInd)
%  18  2012-07-05        BryanP      Integrated "smart" sampled boostrap
%  19  2012-07-06 16:55  BryanP      Added time to PostToVfun calls


%===========================%
%%     Handle Defaults      %
%===========================%

%Support passing pre-build structures or cell arrays in for options
if length(varargin) == 1
    varargin = varargin{:};
end

adp_defaults = {
                'parallel'              false
                'max_iter'              1000
                'sample_per_iter'       10
                'verbose'               50
                'max_time_sec'          Inf

                'vfun_init_alg'        	[]
                'explore_iter'          100
                'explore_alg'           []
                'sbi_samples'           100     %vector of samples per period or scalar for uniform

                'boot_iter_per_t'       1
                'boot_sample_state'     true
                'boot_enable_full_sim'  true

                'vfun_approx'           'LocalRegr'
                'vfun_setup_params'     {}
                'vfun_approx_params'    {}
                'vfun_pts_over_dim'     2

                'tol_bkps_val'          1e-4
                'bkps_use_updated_vfun' false
                'bkps_abort'            true
                'bkps_abort_by_dec'     false
                'converge_bkps'         true

                'fix_rand'              false
                'fix_rand_is_done'      false
                'decfun_params'         {}
                'old_results'           []      %Existing results (including value function) to start from
               };

adp = SetDefaultOpts(varargin, adp_defaults);


% code to get consistent random numbers during debugging
if adp.fix_rand && not(adp.fix_rand_is_done)
    disp('  RAND RESET: adpTD1 using fixed random number stream')
    % Reset the random number generator
    rng('default');

    %Old (pre-R2011a) syntax
 	%rand_num_sequence = RandStream('mt19937ar');
 	%RandStream.setDefaultStream(rand_num_sequence)

    %Indicate that we have already reset the random number generator to
    %prevent sub-functions from resetting it again
    adp.fix_rand_is_done = true;
end

%===========================%
%%     Initialization       %
%===========================%

if adp.verbose
    fprintf('\nRunning Temporal Difference with lambda=1 (aka double pass)\n')
    if adp.parallel
        par_string = 'on';
    else
        par_string = 'off';
    end
    if isempty(adp.old_results) ...
        || not(isfield(adp.old_results, 'n_iter')) ...
        || adp.old_results.n_iter <=0
        resume = false;
        restart_string = '';
    else
        resume = true;
        restart_string = ' added';
    end

    fprintf('    max %d%s iterations, %d samples/iter\n', ...
                adp.max_iter, restart_string, adp.sample_per_iter)
    fprintf('    timeout %d sec, parallel %s\n', ...
                adp.max_time_sec, par_string)
end

%---- Setup timer
parTD_time = tic;

%% ---- Setup problem specific functions
% Notes:
%   - third column specifies the default function to use, [] if the
%      function is optional, or NaN if a problem specific function is
%      required
%   - this setup handles problems specified as objects or structures

fn.OptimalDec = FunctForProblem(problem, 'fOptimalDec', @FindOptDecFromVfun);
fn.FullSim    = FunctForProblem(problem, 'fFullSim',    []);
fn.DecCompare = FunctForProblem(problem, 'fDecCompare', @isequaln);
fn.ValCompare = FunctForProblem(problem, 'fValCompare', @RelValCompare);
fn.Sim        = FunctForProblem(problem, 'fSim',        NaN);
fn.ApplyDscn  = FunctForProblem(problem, 'fApplyDscn',  NaN);
fn.PostToVfun = FunctForProblem(problem, 'fPostToVfun', []);
fn.FirstState = FunctForProblem(problem, 'fFirstState', NaN);

results.log.backpass_abort = zeros(1, problem.n_periods+1);

%% ===== SETUP VALUE FUNCTION =======
%---- Setup value function
if isempty(adp.old_results)

    vfun_constructor = str2func(['fa' adp.vfun_approx]);
    %Create array of value functions. Reverse loop to pre-allocate size
    for t = problem.n_periods+1:-1:1
        %Note place holders for initial point and value lists
        vfun(t) = vfun_constructor([],[], adp.vfun_setup_params{:});
        if isfield(problem.state, 'names')
            vfun(t).PtDimNames = problem.state.names(problem.state.mask.vfun(min(t,end),:));
        end
    end

    if not(isempty(adp.vfun_init_alg))
        % If requested Initial Value function approximation using start_algorithm
        start_alg = str2func(adp.vfun_init_alg);

        % Run the starting algorithm
        if adp.verbose
            fprintf('Pre-populate value functions using ')
        end
        vfun = start_alg(problem, adp, vfun, fn);

        %Check for time out
        if toc(parTD_time) > adp.max_time_sec
            if adp.verbose
                fprintf('Timeout\n')
            end
            results.vfun = vfun;
            results.is_converged = false;
            results.n_iter = 0;
            return
        end
    end

    iter_start = 0;

else
    if isfield(adp.old_results, 'n_iter')
        iter_start = adp.old_results.n_iter;
    else
        iter_start = 0;
    end

    if adp.verbose
        if resume
            fprintf('Resuming at iteration #%d\n', iter_start)
        end
        fprintf('Using (copy of) existing Value Function\n')
    end
    %Make a true, local copy of the value function (since they are handle
    %objects. Otherwise, our additions will effect the stored results)
    for t = problem.n_periods+1:-1:1
        vfun(t) = copy(adp.old_results.vfun(t));
    end

    %Simplify old_results to streamline our options structure
    adp.old_results = true;
end

% % % Setup explore function
% % if not(isempty(adp.explore_alg))
% %     explore_func = str2func(adp.explore_alg);
% % end
%

%=========================================%
%%   Run ADP (TD, lambda=1 /double pass)  %
%=========================================%
%Setup iteration loop related storage
pre_s_list = NaN(problem.n_periods + 1, problem.dims.pre_state, adp.sample_per_iter);
post_s = NaN(problem.n_periods + 1, problem.dims.post_state, adp.sample_per_iter);
decision = NaN(problem.n_periods + 1, problem.dims.decision, adp.sample_per_iter);

dec_contrib = zeros(adp.sample_per_iter, problem.n_periods + 1);
sim_contrib = zeros(adp.sample_per_iter, problem.n_periods + 1);
fwd_val = zeros(adp.sample_per_iter, problem.n_periods + 1);

%---- Setup initial state & state storage
[first_pre_state, first_full_state] = fn.FirstState(problem);

disc_factor = 1-problem.disc_rate;

if adp.verbose
    fprintf('AdpTD1:')
end

% Duplicate convergence flag, for cases where max_iter = 0
is_converged = false;


% ADP outer iteration loop
for iter = (iter_start+1):(iter_start + adp.max_iter)

    % Convergence flag to break out before n_iter.
    is_converged = false;

    %print out progress indicator dots
    DisplayProgress(ceil(adp.verbose/adp.sample_per_iter), iter, [])

%     if mod(iter, adp.explore_iter) == 0
%         %% ===== EXPLORATION =======
%         if adp.verbose
%             fprintf('Full explore using ')
%             % Note: explore_func should identify itself with verbose and
%             % finish with a new line.
%         end
%         % Every so often, explore value space
%         vfun = explore_func(problem, adp, vfun);
%
%         % Flag that we have no backpass convergence information
%         backpass_ok = false;
%         if adp.verbose
%             fprintf('adpTD1 (con''t)')
%         end
%     else

        %% ===== FORWARD PASS =======

        %Setup bootstrap
        if iter <= (problem.n_periods + 1) * adp.boot_iter_per_t
            in_bootstrap = true;
            t_start = problem.n_periods+1 - floor((iter-1)/adp.boot_iter_per_t);

            if adp.boot_sample_state
                pre_start = sample(problem.rand.state{t_start}, adp.sample_per_iter);
                full_start = mat2cell(pre_start,ones(adp.sample_per_iter,1));
            else
                error('ADP:NotReady','Rolling decision sampling to reach pre_state not ready')
            end

            if t_start <= problem.n_periods
                [start_decision, start_post] = ...
                    sample(problem.rand.decision{t_start}, pre_start, full_start);
            else
                start_decision = zeros(adp.sample_per_iter,1);
                start_post = fn.ApplyDscn(problem, pre_start, 0, t_start);
            end
        else
            in_bootstrap = false;

            t_start = 1;
            pre_start = repmat(first_pre_state,adp.sample_per_iter,1);
            full_start = num2cell(repmat(first_full_state,adp.sample_per_iter,1),2);
        end

        if adp.parallel
            parfor s_path = 1:adp.sample_per_iter
                [decision(:, :, s_path), dec_contrib(s_path, :), post_s(:, :, s_path),...
                 fwd_val(s_path, :), pre_s_list(:, :, s_path), sim_contrib(s_path, :)] ...
                    = ForwardPassCore(problem, fn, vfun, adp, ...
                        t_start, pre_start(s_path,:), full_start{s_path}, ...
                        in_bootstrap, start_decision(s_path,:), start_post(s_path, :));
            end
        else
            for s_path = 1:adp.sample_per_iter
                [decision(:, :, s_path), dec_contrib(s_path, :), post_s(:, :, s_path),...
                    fwd_val(s_path, :), pre_s_list(:, :, s_path), sim_contrib(s_path, :)] ...
                    = ForwardPassCore(problem, fn, vfun, adp, ...
                        t_start, pre_start(s_path,:), full_start{s_path}, ...
                        in_bootstrap, start_decision(s_path,:), start_post(s_path, :));
            end
        end


        %% ===== FULL SIMULATION OF MULTIPLE PATHS
        %Usage cases:
        % 1) With separate operations approximation, this allows us to use
        % the current approximation in the forward pass, and then to update
        % the approximation & associated contributions all at once
        %
        % 2) If it is possible to run an approximate, incremental forward
        % simulation with fn.Sim, but a full sophisticated model requires
        % simulating the entire time period at once
        if not(isempty(fn.FullSim)) && (not(in_bootstrap) || adp.boot_enable_full_sim)
            [sim_contrib, problem] = ...
                fn.FullSim(problem, [], pre_s_list, decision, sim_contrib, post_s, adp);
        end

        %% ===== BACKWARD PASS =======
        % Note: terminal period treated like any other b/c desc costs set
        % to zero

        backpass_ok = true(adp.sample_per_iter, 1);

        %Now loop over all previous periods and update the value functions
        for t = problem.n_periods+1:-1:t_start
            %Convert postdec states for current time period to vfun
            %space (if needed)
            vfun_state = post_s(t, :, :);
            %Reshape required b/c slicing from 3-D
            vfun_state = reshape(vfun_state, [], adp.sample_per_iter)';
            if not(isempty(fn.PostToVfun))
                vfun_state = fn.PostToVfun(problem, vfun_state, t);
            end

            % The terminal and second-to-last periods are special b/c their
            % post-decision state only includes simulation results and not
            % decision contributions.
            % They also share the same simulation results (terminal period
            % values) but match these to different states: the n_periods+1
            % pre-decision state for the terminal period and the n_periods
            % post-decision state for the final decision period.
            %
            % Therefore:
            if t >= problem.n_periods
                % Initialize/reset actual value accumulator for terminal period
                % and then again for 2nd to last to avoid double counting
                % sim_contrib
                actual_vals = zeros(adp.sample_per_iter, 1);
                contrib = sim_contrib(backpass_ok, t);
%             elseif t == problem.n_periods
%                 actual_vals = zeros(adp.sample_per_iter, 1);
%                 contrib = sim_contrib(backpass_ok, t+1);
            else
            % Otherwise, the actual_vals has already been loaded with the
            % next period actual values and we need to compute a
            % contribution that is a function of the next-period decision
            % and next-period simulation. No discounting required b/c the
            % postdecision value functin is stored in t+1 money
                contrib = sim_contrib(backpass_ok, t+1)...
                           + dec_contrib(backpass_ok, t+1);
            end

            % If we opt to use the updated value function approximation on the
            % backpass, first add the observation and then look up these values
            % Note: current period (t) value function stored in t+1
            % dollars, but need to discount actual (future) values since
            % they are in t+2 money
            if adp.bkps_use_updated_vfun && not(in_bootstrap)

                % Update the (good) results from the forward pass
                val_to_update = contrib + disc_factor * actual_vals;
                %use ok mask to remove any NaNs to preven NaN in update call
                backpass_ok = backpass_ok & not(isnan(val_to_update));

                update(vfun(t), vfun_state(backpass_ok, :), ...
                    val_to_update(backpass_ok));
                % Find updated approximation for all (good & bad) forward
                % pass states
                actual_vals = approx(vfun(t), vfun_state, adp.vfun_approx_params{:});

            else
                % Otherwise, use the observed values and then update the value
                % function
                actual_vals(backpass_ok) = ...
                    contrib + disc_factor * actual_vals(backpass_ok);

                %use ok mask to remove any NaNs to preven NaN in update call
                backpass_ok = backpass_ok & not(isnan(actual_vals));
                update(vfun(t), vfun_state(backpass_ok, :), actual_vals(backpass_ok));
            end

            % Handle backpass abort for each simulation path
            if adp.bkps_abort && not(in_bootstrap)
                new_backpass_ok = backpass_ok;
                if adp.bkps_abort_by_dec && t <= problem.n_periods
                %Note: no decisions in the terminal state: check those values 
                    
                %For abort by decision, flag any decisions that have
                %changed to much as not OK
                    if adp.parallel
                        parfor s_path = 1:adp.sample_per_iter
                            if backpass_ok(s_path)
                                new_backpass_ok(s_path) = ...
                                	BackPassDecCheckCore(problem, fn, vfun(t), adp, t, ...
                                        pre_s_list(t, :, s_path), decision(t, :, s_path)); %#ok<PFBNS>
                            end
                        end
                    else
                        for s_path = 1:adp.sample_per_iter
                            if backpass_ok(s_path)
                                new_backpass_ok(s_path) = ...
                                	BackPassDecCheckCore(problem, fn, vfun(t), adp, t, ...
                                        pre_s_list(t, :, s_path), decision(t, :, s_path));
                            end
                        end
                    end
                else
                    % Otherwise we check our forward pass vs the current
                    % (post update) values
                    if adp.bkps_use_updated_vfun
                        new_vals = actual_vals(backpass_ok);
                    else
                        new_vals = approx(vfun(t), vfun_state(backpass_ok, :), adp.vfun_approx_params{:});
                    end

                    new_backpass_ok(backpass_ok) = fn.ValCompare(new_vals, fwd_val(backpass_ok, t), adp.tol_bkps_val);

                end
                results.log.backpass_abort(t) = results.log.backpass_abort(t)...
                    + nnz(backpass_ok) - nnz(new_backpass_ok);
                backpass_ok = new_backpass_ok;

                % If we no longer have any paths to update, fully abort the
                % backpass
                if not(any(backpass_ok))
                    break
                end
            end

        end
%     end

    %% ===== END OF ITERATION CALCS & CHECKS =======
    %---- Check for convergence
    % If we haven't converged on decisions for the backpass, assume we
    % haven't converged
    if not(in_bootstrap) && adp.converge_bkps && all(backpass_ok)
        if adp.verbose
            fprintf('BkPs_')
        end
        is_converged = true;
    end

    % Log progress

    if is_converged
        if adp.verbose
            fprintf('Converged (%d)\n', iter)
        end
        break
    else
        %Check for time out
        if toc(parTD_time) > adp.max_time_sec
            if adp.verbose
                fprintf('Timeout\n')
            end
            break
        end
    end
end

if adp.verbose && not(is_converged) && not(isempty(iter)) && iter == adp.max_iter
    fprintf('\nIteration Limit (%d) reached\n', iter)
end


results.vfun = vfun;
results.adp = adp;
results.is_converged = is_converged;
results.n_iter = iter;
[results.first_desc, dec_contrib, ~, cost_to_go] = fn.OptimalDec(problem, 1, first_pre_state, vfun(1), adp);
results.objective = dec_contrib + disc_factor * cost_to_go;

end

%% ============ Helper Functions =============
%----------------------
%   ForwardPassCore
%----------------------
function [decision, dec_contrib, post_s, fwd_val, pre_s_list, sim_contrib] ...
            = ForwardPassCore(problem, fn, vfun, adp, ...
                              t_start, pre_start, full_start, ...
                              in_bootstrap, start_decision, start_post)
% "Insides" of ForwardPass to be shared by for & parfor loops

    %Setup iteration loop related storage
    pre_s_list = NaN(problem.n_periods + 1, problem.dims.pre_state);
    post_s = NaN(problem.n_periods + 1, problem.dims.post_state);
    decision = NaN(problem.n_periods + 1, problem.dims.decision);

    dec_contrib = NaN(1, problem.n_periods + 1);
    sim_contrib = NaN(1, problem.n_periods + 1);
    fwd_val = NaN(1, problem.n_periods + 1);

    full_state = full_start;
    pre_s_list(t_start, :) = pre_start;

    for t = t_start:problem.n_periods
        pre_s = pre_s_list(t, :);

        %Select decision, associated decision contribution (in
        %current t money) and (estimated) post-decision value (in t+1
        %money)
        if in_bootstrap && (t==t_start)
            decision(t, :) = start_decision;
            post_s(t, :) = start_post;
        else
            [decision(t, :), dec_contrib(t), post_s(t, :), fwd_val(t)] ...
                = fn.OptimalDec(problem, t, pre_s, vfun(t), adp);
        end

        % Simulate forward either by
        %  -- realizing stochastic variables to advance from post_state
        %      to the next pre_state
        %   OR
        %  -- using this decision to simulate the full_state (pre)
        %      forward accounting for any stochasticity
        % In both cases, both the pre_state the revised full_state must
        % be returned, although the user is open to supplying
        % placeholder or empty values if they are not required for a
        % specific problem. In addition, the Sim function must compute
        % the simulation portion of the contribution (decision portion
        % of contribution computed with the OptimalDec)
        %
        % Note: the contribution values may be re-computed/updated/
        % over-written if a FullSim function is provided.
        [pre_s_list(t+1, :), sim_contrib(t), full_state]...
            = fn.Sim(problem, t, decision(t, :), post_s(t, :), full_state);

    end

    %% ===== TERMINAL PERIOD (Forward Pass)
    t = problem.n_periods + 1;
    % Map predecision space to post decision space using a zero
    % decision since no decisions to make in terminal period. This also
    % results in on a single possible "post" decision state and
    % corresponding forward pass estimated value so we can assign it
    % directly
    post_s(t, :)...
        = fn.ApplyDscn(problem, pre_s_list(t, :), 0, t);
    %Compute forward values
    if isempty(fn.PostToVfun)
        vfun_state_list = post_s(t, :);
    else
        vfun_state_list = fn.PostToVfun(problem, post_s(t, :), t);
    end

    if not(in_bootstrap)
        fwd_val(t) = vfun(t).approx(vfun_state_list, adp.vfun_approx_params{:});
    end

    % Now simulate to find the actual values
    [~, sim_contrib(t), ~]...
        = fn.Sim(problem, t, 0, post_s(t, :), full_state);
    % Note: no need to set decision cost to zero, b/c it was already
    % initialized that way.

end

%--------------------------
%   BackPassDecCheckCore
%--------------------------
function new_backpass_ok = ...
    BackPassDecCheckCore(problem, fn, vfun, adp, t, pre_s, old_dec)
    new_dec = fn.OptimalDec(problem, t, pre_s, vfun, adp);
    if not(fn.DecCompare(new_dec, old_dec))
        new_backpass_ok(s_path) = false;
    end
end


%--------------------
%   RelValCompare
%--------------------
function t_or_f = RelValCompare(a, b, tol)
% Helper function for simple relative value compare with divide by zero
% work around
    t_or_f =  abs(a-b)./max(max(max(abs(a),[],2),max(abs(b),[],2)),1e-3) < tol;
end
