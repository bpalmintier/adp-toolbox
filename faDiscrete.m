classdef faDiscrete < FuncApprox_old
%WARNING NOT UPDATED FOR NEW FuncApprox
%
% DOES NOT WORK
%
%FADISCRETE Discrete function approximation for approx. dynamic programming
%
% Discrete function approximation for use with approximate dynamic
% programming and other simulations. For this class, the state must have
% already been translated (or naturally be) the set of integers between zero and the maximumthe
% range for each dimension
%
% originally by Bryan Palmintier 2011

% HISTORY
% ver     date    time       who     changes made
% ---  ---------- -----  ----------- ---------------------------------------
% 0.1  2011-03-17 19:20  BryanP      Initial Version
% 0.2  2011-03-24 12:21  BryanP      Debug multiple state indexing
% 0.3  2011-03-24 13:45  BryanP      It works! (based on manual checking)
% 0.4  2011-03-24 15:45  BryanP      Extracted mat2ind for general use
% 0.5  2011-03-24 20:31  BryanP      Further debugs & new copy() method
% 0.6  2011-03-25 18:21  BryanP      Added raw() method
%   7  2012-03-25 15:45  BryanP      Overhauled for FuncApprox v4 


    properties (Access=protected)
        %Value and meta-data storage as a collection of n-D arrays
        Value = [];   % current function approximation for discrete item
        Count = [];   % number of updates for each item
        Step  = [];   % current step_size for each item
        Max_per_dim = []; % easy access to the maximum per dimension
    end
    
    properties (SetAccess=protected)
        Step_fun = @ss1overN; %stepsize convergence function
        Step_opt = []; %stepsize function options
        Range = zeros(2,0);  %two row vector of minimum (1,:) and maximum (2,:) extents
        Init = 0;
    end
    
    methods
        %======= Constructor =====
        function obj = faDiscrete(max_per_dim, Init)

            %Support blank constructor by providing an empty max_per_dim.
            %In most cases, a blank constructor is used only for special
            %purposes such as copying and restoring from save, such that a
            %valid non-empty value will be copied over this setting.
            %
            % In the future, a blank constructor may also be used with an
            % auto-expanding approximation as well.
            if nargin < 1
                max_per_dim = [];
            end
            % Set up size related meta-data
            obj.Max_per_dim = max_per_dim;
            obj.N_PtDim = length(max_per_dim);
            
            % If we have a valid max_per_dim, use it to Initialize other
            % storage arrays
            if not(isempty(max_per_dim))
                obj.Range = ones(2, obj.N_PtDim);
                obj.Range(2,:) = max_per_dim;
            
                % Initialize value storage
                if nargin > 1 && not(isempty(Init))
                    obj.Init = Init;
                end
                %Note: if Init not defined, we inherit the default from
                %FuncApprox
                
                obj.Value = obj.Init*ones(max_per_dim);

                % Initialize value related metadata
                obj.Count = zeros(max_per_dim);
                obj.Step = ones(max_per_dim);
            else
                % if max_per_dim is empty, we just need to Initialize some
                % empty storage
                
                % In this case the default range from FuncApprox is used
                obj.Value = [];
                obj.Count = [];
                obj.Step = [];
            end
        end

        %======= Standard FuncApprox functions =====
        function [values, stds, steps] = approx(obj, states)
        % [values, stds, steps] = obj.approx(states)
        %
        % Approximate the value for a given list of inputs (the states). Each
        % state should be a numeric row vector such that a matrix (with one
        % state per row).
            
            % Find linear indicies from the state list
            idx = mat2ind(size(obj.Value),states);

            % Extract the values
            values = obj.Value(idx);
            
            %TODO: handle standard deviations (or variances)
            % Standard deviations are not currently stored/computed, so return NaN
            stds = NaN*ones(size(values));
            
            %return current step sizes
            steps = obj.Step(idx);
        end
        
        function [values, stds, steps] = update(obj, states, values)
        % [values, stds, steps] = obj.update(states, values)
        %
        % Upates the value(s) for (a) given state(s). If needs_jacobian is
        % true, callers are expected to pass a valid jacobian. For lists of
        % states, the jacobian is passed as a cell column vector

            % Find linear indicies from the state list
            idx = mat2ind(size(obj.Value),states);
            
            obj.Count(idx) = 1 + obj.Count(idx);
            obj.Step(idx) = obj.Step_fun(obj.Count(idx), obj.Step(idx), obj.Step_opt);
            obj.Value(idx) = (1 - obj.Step(idx)) .* obj.Value(idx) + obj.Step(idx).*values;
            
            %if resultilng values, etc. are requested, use approx to find
            %them
            if nargout > 0
                [values, stds, steps] = approx(obj, states);
            end
            
        end
        
        % max()
        %
        % Overload the built-in max function for our class.
        %
        % locates the (constrained) maximum of the approximation within the
        % range specified by lo and hi limits. The lo & hi limits should
        % take the same form as states (row vector of values). Only one set
        % of limits is allowed. Omiting lo & hi limits finds the global
        % maximum over the defined range of the approximation
        function [max_val, state, std] = max(obj, hi_limit, lo_limit)
            
            if nargin < 2 || isempty(hi_limit)
                hi_limit = obj.Max_per_dim;
            end
            
            if nargin < 3 || isempty(lo_limit)
                lo_limit = ones(size(hi_limit));
            end

            % Start by converting states matrix into a string to use for
            % indexing with eval for subscripting
            idx_string = obj.limitsToString(lo_limit, hi_limit);

            % All of the hard work will be done using the findMaxHelper
            % function, but in some cases we can make the job easier and
            % faster by adjusting the number of input and output arguements
            % to match what are passed/required
            %
            % Implementation note: here we use eval to handle arbitrary
            % indexing based on idx_string.

            if nargout == 1
                % Skip finding the arg max if it is not needed
                max_val = obj.findMaxHelper(eval(['obj.Value(' idx_string ')']), lo_limit - 1);
            else
                [max_val, state] = obj.findMaxHelper(eval(['obj.Value(' idx_string ')']), lo_limit - 1);
            end
            
            if nargout > 2
                %TODO: actually implement standard deviations
                std = NaN*ones(size(max_val));
            end

            
        end

        % maxScaledSum
        % locates the (constrained) maximum of a scaled sum of two
        % approximations of the same subclass within the specified range.
        % This is effectively:
        %   MaxScaledSum = max(scale(1)*obj1 + scale(2)*obj2)
        %
        % This function is often called directly without the . notation as
        % in:
        %   maxScaledSum(obj1, obj2)
        %
        % obj1 & obj2 must be either numeric multi-dimensional arrays or
        % objects of the faDiscrete class (or sub-classes). The dimensions
        % of obj1&2 are assumed equal, although it is possible to use two
        % unequally sized objects if the limits are valid for both objects.
        %
        % Scale is an optional 2 element vector and assumed to be [1 1] if 
        % omitted
        %
        % The lo & hi limits should take the same form as states (row
        % vector of values). Only one set of limits is allowed. Omiting lo
        % & hi limits finds the global maximum over the defined range of
        % the approximation
        function [max_val, state, std1, std2] = maxScaledSum(obj, obj2, scale, hi_limit, lo_limit)
            
            % Handle input defaults
            if nargin < 3 || isempty(scale)
                scale = [1 1];
            end
            
            if nargin < 4 || isempty(hi_limit)
                hi_limit = obj.Max_per_dim;
            end
            
            if nargin < 5 || isempty(lo_limit)
                lo_limit = ones(size(hi_limit));
            end
            
            % -- First build up a data array with the limited, scaled sum of
            % our inputs
            
            % Start by converting states matrix into a string to use for
            % indexing with eval for subscripting
            idx_string = obj.limitsToString(lo_limit, hi_limit);
            
            % Create our data multi-d array from the first object (assumed
            % to be an faDiscrete object
            %
            % Implementation note: here we use eval to handle arbitrary
            % indexing based on idx_string.
            data = scale(1) .* eval(['obj.Value(' idx_string ')']);
            
            % Now add in our second object's values
            if isa(obj2, 'faDiscrete')
                data = data + scale(2) .* eval(['obj2.Value(' idx_string ')']);
            elseif isnumeric(obj2)
                % if obj2 is empty, allow us to use this to scale the
                % single Initial object by not changing data
                if not(isempty(obj2))
                    if isscalar(obj2)
                        data = data + scale(2) * obj2;
                    else
                        if ndims(obj2) < length(lo_limit)
                        %Handle possible trailing degenerate dimensions
                            missing_dim = length(lo_limit) - ndims(obj2);
                            idx_string = obj.limitsToString(...
                                            lo_limit(1:(end - missing_dim)), ...
                                            hi_limit(1:(end - missing_dim)) );
                        end
                        data = data + scale(2) .* eval(['obj2(' idx_string ')']);
                    end
                end
            else
                error('maxScaledSum:invalidClass',...
                    'maxScaledSum only supports numeric and faDiscrete based objects')
            end

            % Just as in our overloaded max(), all of the hard work will be
            % done using the findMaxHelper function, but in some cases we
            % can make the job easier and faster by adjusting the number of
            % input and output arguements to match what are passed/required
            if nargout == 1
                % Skip finding the arg max if it is not needed
                max_val = obj.findMaxHelper(data, lo_limit - 1);
            else
                [max_val, state] = obj.findMaxHelper(data, lo_limit - 1);
            end
            
            if nargout > 2
                %TODO: actually implement standard deviations
                std1 = NaN*ones(size(max_val));
                std2 = std1;
            end
            
        end
        
        function [raw_data] = raw(obj)
        % raw
        % Returns the raw value function approximation
        % There is no need to maintain compatibility between versions
            raw_data = raw@FuncApprox(obj);
            
            raw_data.Value = obj.Value;
            raw_data.Count = obj.Count;
            raw_data.Step  = obj.Step;
            raw_data.Max_per_dim = obj.Max_per_dim; % easy access to the maximum per dimension

        end
        
        
    end

    methods (Access = protected)
        %% ===== Helper functions
        % Find the max and argmax with a multi-D array
        function [max_val, state] = findMaxHelper(obj, data, offset)
            
            %Find maximum
            % Implementation note: the use of (:) automatically converts to
            % linear indexing and includes all elements (rather than just
            % the first dimension in a single max statement.
            max_val = max(data(:));
            
            %Find argmax if requested
            if nargout > 1
                % Find the (first) maximum
                idx = find(data(:) == max_val, 1);
                
                % preconstruct a cell array to receive the multiple
                % dimensions of our subscript
                state = num2cell(zeros(1,obj.N_PtDim));
                
                % Convert the linear find index to subscripts and assign to
                % each element of the cell array
                [state{:}] = ind2sub(size(data), idx);
                
                % Finally convert the cell array to a numeric row vector
                state = horzcat(state{:});

                % The above find statement locates the maximum within
                % the sub-region specified by lo:hi limits so we need
                % to offset our state
                %
                % However, if there was no value found we want to avoid
                % raising an error
                if not(isempty(state))
                    state = state + offset;
                end
            end
        end
        
        % Convert two equally sized limit vectors into a string that can be
        % used for discrete indexing.
        function idx_string = limitsToString(obj, lo_limit, hi_limit) %#ok<MANU>
                % First build up the arbitrarily long index list
                idx_string = sprintf('%d:%d,', vertcat(lo_limit(1:end-1),hi_limit(1:end-1)));
                idx_string = [idx_string sprintf('%d:%d', lo_limit(end),hi_limit(end))];
        end
        
        %% ===== Property value maintenance 
    end
    
end

