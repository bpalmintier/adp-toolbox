classdef SampleNdRange
%SAMPLENDRANGE class for sampling across multiple dimensions
%
% Examples:
%   sample_minmax = [0  0   5   3.4   5.6
%                    4  4  34   4.6   52  ];
%   s = SampleNdRange(sample_minmax)
%   s.sample(10)
% 
%   sample_step = [1 0 3 1.2 0];   %Zero implies continous sample
%   s2 = SampleNdRange(sample_minmax, sample_step)
%   s2.sample(10)
%   
%   s3 = SampleNdRange(sample_minmax, sample_step, 'sobol')  %Default is psuedo-random
%   s3.sample(10) %10 randomly scrambled 5-D sobol samples over range
%
% originally by Bryan Palmintier 2012

% HISTORY
% ver     date    time       who     changes made
% ---  ---------- -----  ----------- ---------------------------------------
%   1  2012-03-20 13:40  BryanP      Initial Version
%   2  2012-04-02 11:40  BryanP      Expand scalar step to all dims
%   3  2012-04-19 21:15  BryanP      Convert sample to use bsxfun()
%   4  2012-06-10 00:45  BryanP      BUGFIX: support for 1-D samples

    % Properties the world can read and write
    properties
        name;   %optional name
    end

    % Read-only properties (set by constructor)
    properties (SetAccess='protected')
        SampleType = [];
        N_dim = [];
        MinMax = [];
        Step = [];
    end
    
    % Internal read-only properties
    properties (Access='protected')
        SampleSet = [];
        fRawSample = [];   % (Quasi-)random sample function
        SampleRange = [];
        DMask = [];
        HasDiscrete = false;
        SampleOffset = [];
    end
    
    methods
        %% ===== Constructor
        function obj = SampleNdRange(sample_minmax, sample_step, sample_type)
            % Support zero parameter calls to constructor for special
            % MATLAB situations (see help files)
            if nargin > 0
%                 if iscolumn(sample_minmax)
%                     sample_minmax = sample_minmax{1,:}';    %Take first row and convert to column
%                 end
                obj.MinMax = sample_minmax;
                %Compute the sample range between min (first row) and max
                %(second row) using range = max-min
                obj.SampleRange = diff(obj.MinMax,1, 1);
                obj.SampleOffset = obj.MinMax(1,:);
                
                obj.N_dim = size(obj.MinMax,2);
                
                if nargin < 2 || isempty(sample_step)
                    sample_step = zeros(1, obj.N_dim);
                end
                %If scalar step, assume it applys to all dimensions
                if isscalar(sample_step)
                    sample_step = ones(1, obj.N_dim) * sample_step;
                end
                obj.Step = sample_step;
                %Set up a boolean mask for any discrete variables
                obj.DMask = (obj.Step ~= 0);
                
                if any(obj.DMask)
                    obj.HasDiscrete = true;
                    
                    %-- round MinMax range to match stepsize
                    %Convert the discrete sample range to a set of integers
                    d_min = ceil(obj.MinMax(1,obj.DMask)./obj.Step(obj.DMask));
                    d_max = floor(obj.MinMax(2,obj.DMask)./obj.Step(obj.DMask));

                    %Store range and add one to include both end points.
                    obj.SampleRange(obj.DMask) = d_max - d_min + 1;
                    obj.SampleOffset(obj.DMask) = d_min .* obj.Step(obj.DMask);
                end
                
                if nargin < 3 || isempty(sample_type)
                    sample_type = 'rand';
                end
                obj.SampleType = sample_type;
                
                switch lower(sample_type)
                    case 'rand'
                        obj.fRawSample = @(N) rand(N, obj.N_dim);
                    case 'sobol'
                        obj.SampleSet = sobolset(obj.N_dim);
                        obj.SampleSet = scramble(obj.SampleSet, 'MatousekAffineOwen');
                        
                        % Setup function to provide a series of quasi-random state samples
                        obj.fRawSample = @(N) qrand(qrandstream(obj.SampleSet),N);
                    otherwise
                        error('AdpSample:TypeUnkown', 'Unknown sample type %s',sample_type)
                end
                
            end
        end
        
        %% ===== Sample Method
        
        function out = sample(obj, N, varargin) 
            % SAMPLE Return the requested number of multi-dimensional samples
            %
            % out = NdSampleObject.sample(N)
            if nargin < 2 || isempty(N)
                N = 1;
            end
            out = obj.fRawSample(N);

            % -- Transform from [0,1] to desired sample_minmax
            out = bsxfun(@times, out, obj.SampleRange);
            if obj.HasDiscrete
                out(:,obj.DMask) = bsxfun(@times, ...
                    floor(out(:,obj.DMask)), obj.Step(:,obj.DMask));
            end
            out = bsxfun(@plus, out, obj.SampleOffset);
            
        end
    end
    
end

