function [vfun, results] = adpSBI(problem, adp, vfun, fn)
% adpSBI Sampled Backward Induction for estimated ADP and intialization
%
% [vfun, results] = adpSBI(problem, adp, vfun, fn)
%
% Note: This method is only applicable to problem classes where it is
% possible to sample the post decision state without requiring a full,
% simulation derived state (ie it does not work for partially hidden
% Markov/quasi-Markov processes). Use SimBackInd for a universal approach
% with a similar philosophy but that requires more sub-period simulations.
%
%
%
% Required problem attributes
%   n_periods
%   disc_rate
%   dyn_var list
%
% Required problem methods:
%   preToPost: takes decision list and pre_dec state and returns a list
%     of post decision states. This also  allows the user to map to a
%     reduced dimensionality
%   contrib(state_list, t): not counting decision costs
%
% Optional problem methods/functions
%   pre-dec state sample function
%   multi-contribution: compute multiple contribution functions at once.
%     this also allows user to work with a contribution function
%     approximation
%
% Required value function methods
%   update value function: takes a vector of (value based) states and their
%     corresponding value
%
% Adp options:
%   Sample function... pure Monte Carlo, hypercube, etc.
%
% Required sub-functions:
%   Sobol sample(range_by_dimension, n samples
%   Hypercube(dyn_vars

% HISTORY
% ver     date    time       who     changes made
% ---  ---------- -----  ----------- ---------------------------------------
%   1  2012-03-07        BryanP      Pseudo-code
%   2  2012-03-15        BryanP      Original (partial) Code
%   3  2012-03-19        BryanP      Hack in ncw expansion problem
%   4  2012-03-20 16:00  BryanP      Use SampleNdRange for default sample objects
%   5  2012-03-26 16:45  BryanP      Use FuncApprox family
%   6  2012-04           BryanP      Problem specific sample + Refine CapPlan decisions
%   7  2012-04-07 11:25  BryanP      Added verbose
%   8  2012-04-16 17:25  BryanP      Use generic optimal decision function
%   9  2012-04-19 21:45  BryanP      Separate postdec from vfun state spaces
%  10  2012-04-21 17:20  BryanP      Rely on faLocalRegr for auto-expand of neighborhood
%  11  2012-04-22        BryanP      BUGFIX: future costs. AND param/return for nested functions
%  12  2012-05-05 23:55  BryanP      Add adp to results
%  13  2012-05-07 11:22  BryanP      Add dimension names to vfun
%  14  2012-05-08 14:05  BryanP      Renamed from PreToPost to ApplyDscn
%  15  2012-05-11 16:25  BryanP      Fix objective to include discounting and decision contribution
%  16  2012-05-14 12:45  BryanP      Added adp prefix to name
%  17  2012-06-08 12:55  BryanP      New adp defaults: decfun_params
%  18  2012-06-20 10:15  BryanP      Improved handling of bad (too sparse) lookup including sbi_min_ok_for_ev
%  19  2012-06-20/24     BryanP      Convert (par)for loop "Core" functions from nested to true sub-functions (easier debug)
%  20  2012-07-03 14:55  BryanP      Renamed to adpSBI (was adpSampBackInd)
%  21  2012-07-06 16:55  BryanP      Added time to PostToVfun calls


% Maybe: setup inline functions to handle both objects and function handles
% in problem specification

if nargin < 2 || isempty(adp)
    adp = struct([]);
end

adp_defaults = {
                %adpSampleBackInd specific settings
                'sbi_samples'           100
                'sbi_min_ok_for_ev'     0.7     %Min fraction of valid next_predec vals for computing exp_val. setting 0 requires only a single valid val

                %value function defaults
                'vfun_approx'           'LocalRegr'
                'vfun_setup_params'     {}
                'vfun_approx_params'    {}

                'decfun_params'         {}
                %general ADP options used here
                'verbose'               50
                'parallel'              false
                'fix_rand'              false
                'fix_rand_is_done'      false
               };

adp = SetDefaultFields(adp, adp_defaults);

%Setup possibly varying number of samples per period
if length(adp.sbi_samples) < problem.n_periods+1
    adp.sbi_samples(end:(problem.n_periods+1)) = adp.sbi_samples(end);
end
if adp.verbose
    s_num_string = strtrim(sprintf('%g ', adp.sbi_samples/1000));
    fprintf('Sampled Backward Induction ([%s] ksamples/period)\n', s_num_string)
end

% code to get consistent random numbers during debugging
if adp.fix_rand && not(adp.fix_rand_is_done)
    disp('  RAND RESET: adpSBI using fixed random number stream')
    % Reset the random number generator
    rng('default');

    %Old (pre-R2011a) syntax
 	%rand_num_sequence = RandStream('mt19937ar');
 	%RandStream.setDefaultStream(rand_num_sequence)

    %Indicate that we have already reset the random number generator to
    %prevent sub-functions from resetting again
    adp.fix_rand_is_done = true;
end

% Setup value function
if nargin < 3 || isempty(vfun)
    if adp.verbose
        fprintf('    Creating empty value functions (%s)\n', adp.vfun_approx)
    end

    %Remove "empty vfun" to avoid type mismatch issues
    clear('vfun')

    vfun_constructor = str2func(['fa' adp.vfun_approx]);
    for t = 1:problem.n_periods+1
        %Note place holders for initial point and value lists
        vfun(t) = vfun_constructor([],[], adp.vfun_setup_params{:});
        if isfield(problem.state, 'names')
            vfun(t).PtDimNames = problem.state.names(problem.state.mask.vfun(min(t,end),:));
        end
    end
else
    if adp.verbose
        fprintf('    Using (copy of) existing Value Function\n')
    end
    %Make a true, local copy of the value function (since they are handle
    %objects. Otherwise, our additions will effect the stored results)
    for t_idx = problem.n_periods+1:-1:1
        vfun(t_idx) = copy(vfun(t_idx));
    end

    %Flag that we are using old_results (vfun) in our options structure
    adp.old_results = true;
end

%Setup optimal decision function if needed. fn will already be set when
%called from other algorithms for initialization or exploration
if nargin < 4 || isempty(fn)
    fn.OptimalDec = FunctForProblem(problem, 'fOptimalDec', @FindOptDecFromVfun);
    fn.Sim = FunctForProblem(problem, 'fSim', NaN);
    fn.FullSim    = FunctForProblem(problem, 'fFullSim',    []);
    fn.ApplyDscn = FunctForProblem(problem, 'fApplyDscn', NaN);
    fn.PostToVfun = FunctForProblem(problem, 'fPostToVfun', []);
    fn.FirstState = FunctForProblem(problem, 'fFirstState', NaN);
end
fn.ListPossiblePre = FunctForProblem(problem, 'fListPossiblePre', NaN);

% Setup quasi-random stream for deterimining coordinates to sample.
%  We use the low-discrepancy Sobol quasi-random set which more uniformly
%  fills the the unit hypercube than traditional (psuedo) random numbers.
%  After construction we scramble the sequence to help reduce correlation
%  and increase uniformity.
if not(isfield(problem, 'rand'))
    problem.rand = [];
end
if not(isfield(problem.rand, 'state'))
    if adp.verbose
        fprintf('    Creating state sample function using NdRange\n')
    end
    for t = problem.n_periods+1 : -1 : 1    %Start at end for better memory allocation
        problem.rand.state{t} = SampleNdRange(problem.state.range_by_t{t},problem.state.step,'sobol');
    end
end

%-- Sample terminal states (T+1)
t = problem.n_periods + 1;
% Use problem specific sample function if one is defined
if adp.verbose
    fprintf('    T=%d', t)
end

% Sample states
post_state_list = sample(problem.rand.state{t}, adp.sbi_samples(t), 'post');

% -- Compute contribution for sampled states
% Use problem specific multi-state terminal value function if provided
if isfield(problem, 'fMultiTermVal')
    state_values = problem.fMultiTermVal(post_state_list, t);
else
    state_values = zeros(adp.sbi_samples(t),1);

    if isempty(fn.FullSim)
        if adp.parallel
            parfor s = 1:adp.sbi_samples(t);
                state_values(s,:) = ...
                    TermLoopCore(problem, fn, s, t, post_state_list(s,:), adp);
            end
        else
            for s = 1:adp.sbi_samples(t);
                state_values(s,:) = ...
                    TermLoopCore(problem, fn, s, t, post_state_list(s,:), adp);
            end
        end
        if adp.verbose && not(0 == mod(adp.sbi_samples(t), adp.verbose * 50))
                fprintf('%d\n', adp.sbi_samples(t))
        end
    else
        [state_values, problem] = ...
            CPDpFullSim(problem, t, post_state_list, 0, [], post_state_list, adp);
    end
end

%Add these points to the function approximation
if not(isempty(fn.PostToVfun))
    post_state_list = fn.PostToVfun(problem, post_state_list, t);
end
vfun(t).update(post_state_list, state_values);


% Run earlier periods
for t = problem.n_periods:-1:1
    if adp.verbose
        fprintf('    T=%d:', t)
    end

    %--Construct post decision value function
    % Sample intital (full) post-decison states
    % Note: uses problem specific sample function if one is defined

    % Sample post decision states
    post_state_list = sample(problem.rand.state{t}, adp.sbi_samples(t), 'post');
    if adp.verbose
        fprintf('S')
    end
    % Initialize storage
    exp_vals = zeros(adp.sbi_samples(t),1);

    vals            = cell(adp.sbi_samples(t),1);
    prob            = cell(adp.sbi_samples(t),1);

    if not(isempty(fn.FullSim))
        next_pre_by_samp    = cell(adp.sbi_samples(t),1);
        next_opt_dec     = cell(adp.sbi_samples(t),1);
        next_post   = cell(adp.sbi_samples(t),1);
    end

    this_vf = vfun(t+1);
    if adp.parallel
        % -- Parallel using parfor --
        if isempty(fn.FullSim)
            parfor s = 1:adp.sbi_samples(t);
                [vals{s}, prob{s}] = ...
                    BackIndCore(problem, fn, this_vf, s, t, post_state_list(s,:), adp);
            end
        else
            parfor s = 1:adp.sbi_samples(t);
                [vals{s}, prob{s}, next_pre_by_samp{s}, next_opt_dec{s}, next_post{s}] = ...
                    BackIndCore(problem, fn, this_vf, s, t, post_state_list(s,:), adp);
            end
        end
    else
        % -- Non-Parallel --
        if isempty(fn.FullSim)
            for s = 1:adp.sbi_samples(t);
                [vals{s}, prob{s}] = ...
                    BackIndCore(problem, fn, this_vf, s, t, post_state_list(s,:), adp);
            end
        else
            for s = 1:adp.sbi_samples(t);
                [vals{s}, prob{s}, next_pre_by_samp{s}, next_opt_dec{s}, next_post{s}] = ...
                    BackIndCore(problem, fn, this_vf, s, t, post_state_list(s,:), adp);
            end
        end
    end

    if adp.verbose && not(0 == mod(adp.sbi_samples(t+1), adp.verbose * 50))
            fprintf('%d\n', adp.sbi_samples(t))
    end

    % Batch compute contributions if possible
    if not(isempty(fn.FullSim)) && t~=problem.n_periods
        if adp.verbose
            fprintf('Merge states for batch...')
        end

        %Identify unique set of next pre-desc states
        next_pre_list = vertcat(next_pre_by_samp{:});
        [next_pre_list, pre_map] = unique(next_pre_list, 'rows');

        next_opt_dec = vertcat(next_opt_dec{:});
        next_opt_dec = next_opt_dec(pre_map, :);

        next_post = vertcat(next_post{:});
        next_post = next_post(pre_map, :);

        if adp.verbose
            fprintf('Done\n')
        end

        %Simulate contributions for these states
        [sim_contribs, problem] = CPDpFullSim(problem, t+1, next_pre_list, next_opt_dec, [], next_post, adp);

        %Extract the appropriate values for each post state
        for s = 1:adp.sbi_samples(t);
            [~, s_map] = intersect(next_pre_list, next_pre_by_samp{s}, 'rows');
            vals{s} = vals{s} + sim_contribs(s_map, :);
        end

    end

    %Compute EVs
    %Find possible next pre-descision states for each post state
    for s = 1:adp.sbi_samples(t);
        %Flag NaNs for removal
        valid_val = not(isnan(vals{s}));

        % Check sample quality
        %Note using strict > allows sbi_min_ok_for_ev to be set to 0 to
        %require only one valid forward sample
        if nnz(valid_val)/size(vals{s}, 1) > adp.sbi_min_ok_for_ev
            exp_vals(s) = vals{s}(valid_val)' * prob{s}(valid_val);
        else
            exp_vals(s) = NaN;
        end
    end

    %Remove NaNs
    valid_ev = not(isnan(exp_vals));

    % Check sample quality
    if any(valid_ev)
        %Add any valid points to the function approximation
        if not(isempty(fn.PostToVfun))
            post_state_list = fn.PostToVfun(problem, post_state_list(valid_ev,:), t);
        end
        vfun(t).update(post_state_list, exp_vals(valid_ev,:));
    else
        err_msg = sprintf('No valid new vfun points at t=%d. Aborting Sampled Backward Induction', t);
        err_id = 'ADP:SBI:NoValidPts';
        %Warn & abort if no valid new points
        if nargout > 1
            error(err_id, err_msg) %#ok<SPERR> b/c want to reuse msg
        else
            warning(err_id, err_msg) %#ok<SPWRN> b/c want to reuse msg
            break
        end
    end

end


%-- Finished building function approximation

% If full results requested, find the optimial first period build and build
% the results structure
if nargout > 1
    %-- Find optimal build for first period (single state)
    s = fn.FirstState(problem);

    [results.first_desc, desn_contrib, ~, future_val] = fn.OptimalDec(problem, 1, s, vfun(1), adp);

    results.objective = desn_contrib + (1-problem.disc_rate) * future_val;
    results.vfun = vfun;
    results.adp = adp;
end

if adp.verbose && not(0 == mod(adp.sbi_samples(1), adp.verbose * 50))
    fprintf('%d\n',adp.sbi_samples(1))
end

end % Main Function


%% ============ Helper Functions =============
%------------------
%   TermLoopCore
%------------------
function val = TermLoopCore(problem, fn, s_idx, t, state, adp)
% Note: This nested function shares variables with its parent function,
% allowing us to share the "insides" of the forward pass loop for both
% parallel (parfor) and non-parallel (for) uses
    if adp.verbose
        DisplayProgress(adp.verbose,s_idx, '    ')
    end
    [~, val] = fn.Sim(problem, t, 0, state, state);
end

%------------------
%   BackIndCore
%------------------
function [vals, prob, next_pre_list, opt_desc, next_post] = ...
            BackIndCore(problem, fn, vfun, s_idx, t, this_post, adp)
% Note: This function allows us to share the "insides" of the forward
% pass loop for both parallel (parfor) and non-parallel (for) uses
    if adp.verbose
        DisplayProgress(adp.verbose,s_idx, '    ')
    end

    [next_pre_list, prob] = ...
        fn.ListPossiblePre(problem, this_post, t);

    n_next = size(next_pre_list, 1);

    if t==problem.n_periods
        %Final decision period
        opt_desc = zeros(n_next, 1);
        next_post = fn.ApplyDscn(problem, next_pre_list, 0);
        if isempty(fn.PostToVfun)
            next_post_vfun = next_post;
        else
            next_post_vfun = fn.PostToVfun(problem, next_post, t);
        end

        %Find values from the next period value function
        vals = vfun.approx(next_post_vfun, adp.vfun_approx_params{:});
    else
        %Periods before the final decision period (includes neither the
        %terminal period (n_periods+1) nor the final decision period
        %(n_periods)
        vals = NaN(size(prob));
        for next_pre_idx = 1:n_next
            next_pre = next_pre_list(next_pre_idx, :);
            %Find optimal next period decision , associated decision
            %contribution (in t+1 money) and post-decision value (in
            %t+2 money)
            [this_desc, next_dec_contrib, this_next, next_post_val] = ...
                fn.OptimalDec(problem, t+1, next_pre, vfun, adp);

            %TODO: clever allocation?
            if not(isempty(this_desc))
                opt_desc(next_pre_idx,:) = this_desc;
                next_post(next_pre_idx,:) = this_next;
                if isempty(fn.FullSim)
                    %And corresponding non-decision contribution (in T+1 money)
                    [~, next_sim_contrib] = fn.Sim(problem, t+1, this_desc, this_next, next_pre);
                else
                    next_sim_contrib = 0;
                end
                %Compute post-decision value function for this time period
                %(store in t+1 money)
                vals(next_pre_idx) = next_sim_contrib ...
                                      + next_dec_contrib ...
                                      + (1-problem.disc_rate) * next_post_val;
            end
        end
    end
end

